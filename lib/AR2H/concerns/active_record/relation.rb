module AR2H
  module Concerns
    module ActiveRecordConcern
      module Relation
        extend ActiveSupport::Concern

        def present(presenter: :default)
          model = self.model.to_s.pluralize
          ActiveSupport::Notifications.instrument('ar2h.presenting', model: model, presenter: presenter) do
            Executor.run(self, presenter, multiple = true)
          end
        end
      end

      ActiveRecord::Relation.send :include, ActiveRecordConcern::Relation
    end
  end
end

