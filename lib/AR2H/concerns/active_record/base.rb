module AR2H
  module Concerns
    module ActiveRecordConcern
      module Base
        extend ActiveSupport::Concern

        def present(presenter: :default)
          model = self.class
          ActiveSupport::Notifications.instrument('ar2h.presenting', model: model, presenter: presenter) do
            Executor.run(self, presenter)
          end
        end

        def presenters
          self.class.presenters
        end

        module ClassMethods
          def presenter(name = :default, &block)
            presenters.add Presenters::Presenter.new(name, block)
          end

          def presenters
            @presenters ||= Presenters::Collection.new(self.name)
          end
        end
      end

      ActiveRecord::Base.send :include, ActiveRecordConcern::Base
    end
  end
end
