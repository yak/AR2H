module AR2H
  class Executor
    class << self
      def run(object, presenter_name, multiple = false)
        presenter = object.presenters.find(presenter_name)
        unless presenter
          model = multiple ? object.model : object.class
          raise ArgumentError.new("#{model}: presenter #{presenter_name} not found")
        end

        case multiple
          when false
            new(object, presenter).run
          when true
            object.map { |el| new(el, presenter).run }
          else raise ArgumentError.new("Bad multiple argument, expected Boolean but got #{multiple.class}")
        end
      end
    end

    def initialize(object, presenter)
      @object    = object
      @presenter = presenter
      @result    = Hash.new
    end

    attr_reader :object

    def run
      instance_eval(&@presenter.block)
      @result
    end

    private

    def attr(attr, &block)
      @result[attr] = block ? block.call : object[attr]
    end

    def attrs(*attrs)
      attrs.each { |attr| attr(attr) }
    end

    def has_one(model, presenter: nil)
      _has_branch(model, presenter, false)
    end

    def has_many(model, presenter: nil)
      _has_branch(model, presenter, true)
    end

    def belongs_to(model, presenter: nil)
      _has_branch(model, presenter, false)
    end

    def _has_branch(model, presenter, multiple) # :nodoc:
      child_presenter = presenter || @presenter.name
      child_object    = object.send(model)
      @result[model]  = child_object ? Executor.run(child_object, child_presenter, multiple) : nil
    end
  end
end

