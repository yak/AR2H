module AR2H
  module Presenters
    class Collection
      def initialize(model)
        @model      = model
        @presenters = Array.new
      end

      def add(presenter)
        if self.include? presenter
          raise Exception.new("#{@model}: presenter #{presenter.name} is already exists")
        else
          @presenters << presenter
        end
      end

      def all
        @presenters
      end

      def list
        @presenters.map { |presenter| presenter.name }
      end

      def include?(presenter)
        list.include? presenter.name
      end

      def find(name)
        @presenters.find { |presenter| presenter.name == name }
      end
    end
  end
end
