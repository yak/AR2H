module AR2H
  module Presenters
    class Presenter
      def initialize(name, block)
        @name  = name
        @block = block
      end

      attr_reader :name

      def block
        @block ? @block : Proc.new{}
      end

      def eql?(candidate)
        self.name == candidate.name
      end

    end
  end
end



