ActiveSupport::Notifications.subscribe('ar2h.presenting') do |_, started, finished, _, payload|
  time      = (1000.0 * (finished - started)).round(1)
  model     = payload[:model]
  presenter = payload[:presenter]
  message   = "  Presented #{model} as #{presenter} in #{time}ms"
  ActionController::Base.logger.info message
end if defined? ActionController::Base