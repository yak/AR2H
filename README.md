# AR2H

[![build status](https://gitlab.com/yak/AR2H/badges/master/build.svg)](https://gitlab.com/yak/AR2H/commits/master)
[![coverage report](https://gitlab.com/yak/AR2H/badges/master/coverage.svg)](https://gitlab.com/yak/AR2H/commits/master)

**AR2H** is a fast and simple presenter for ActiveRecord.

## Installation

Add this line to your application's Gemfile:
```ruby
gem 'AR2H', '~> 1.0'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install AR2H

## Usage

### Creating presenter
There are no additional classes, just type `presenter` on the model:
```ruby
class User < ApplicationRecord
  has_many :posts
  
  presenter do
    attr :name
    attr :lastname
    attr :age
  end
end
```
Then `User.first.present` will receive hash:
```ruby
{ name: 'Eugene', 
  lastname: 'Yak',
  age: 22 }
```

### Named presenters
If the name of the presenter is omitted, it is automatically called `:default`.
Any model can have multiple presenters:
```ruby
class User < ApplicationRecord
  presenter :for_guest do
    attrs :name, :last_online
  end
  
  presenter :for_vip do
    attrs :name, :email, :friends, :last_online
  end
end
```
The named presenter can be called:
```ruby
User.all.present(presenter: :for_vip)
```

### Attribute with block
The attribute may have a block, which you can use to access the entry object:
```ruby
attr (:name) { "#{object.name} #{object.surname}" }
```
or
```ruby
attr :name do
  name    = object.name
  surname = object.surname
  "#{name} #{surname}" 
end
```

### Relationships
The presenter may have `belongs_to`, `has_one` and `has_many` relationships:
```ruby
class Post < ApplicationRecord
  belongs_to :user
  has_many   :comments
  presenter do
    attr       :body
    belongs_to :user
    has_many   :comments
  end
end
```
this will produce:
```ruby
{ body: 'body', 
  user: {name: 'Eugene Yak'}, 
  comments: [{body: 'Comment 1'}, {body: 'Comment 2'}] }
```

It is worth noting that the sub-model has to be matched with the same presenter. 
To add a different presenetr, do:
```ruby
presenter do
  has_many :comments, presenter: :another_presenter
end
```

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/yak/AR2H.

