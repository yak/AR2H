require 'spec_helper'

describe AR2H::Presenters::Collection do

  let (:collection) { AR2H::Presenters::Collection.new('user') }
  let (:presenter)  { AR2H::Presenters::Presenter.new(:foo, nil) }

  def fill
    collection.add( AR2H::Presenters::Presenter.new(:foo, nil) )
    collection.add( AR2H::Presenters::Presenter.new(:bar, nil) )
    collection.add( AR2H::Presenters::Presenter.new(:baz, nil) )
  end

  it '.initialize' do
    expect(collection.all)
      .to be_empty
  end

  describe '.add' do
    it 'not repeated' do
      expect{ collection.add( AR2H::Presenters::Presenter.new(:foo, nil)) }
          .to change{collection.all.size}.by(1)
    end

    it 'repeated' do
      collection.add( AR2H::Presenters::Presenter.new(:bar, nil) )
      expect{ collection.add( AR2H::Presenters::Presenter.new(:bar, nil)) }
          .to raise_error(Exception, 'user: presenter bar is already exists')
    end
  end

  it '.all' do
    fill
    expect( collection.all )
        .to eql [ AR2H::Presenters::Presenter.new(:foo, nil),
                  AR2H::Presenters::Presenter.new(:bar, nil),
                  AR2H::Presenters::Presenter.new(:baz, nil) ]
  end

  it '.list' do
    fill
    expect( collection.list )
        .to eql [:foo, :bar, :baz]
  end

  describe '.include?' do
    it 'included' do
      fill
      expect( collection.include? presenter )
          .to be_truthy
    end
    it 'not included' do
      fill
      expect( collection.include? AR2H::Presenters::Presenter.new(:woohoo, nil) )
          .not_to be_truthy
    end
  end

  describe '.find' do
    it 'finded' do
      fill
      expect( collection.find :foo )
          .to eql presenter
    end

    it 'not finded' do
      fill
      expect( collection.find :woohoo )
          .to be_nil
    end
  end
end
