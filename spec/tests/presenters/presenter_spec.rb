require 'spec_helper'

PRESENTER = AR2H::Presenters::Presenter

describe PRESENTER do

  let (:presenter) { PRESENTER.new(:foo, Proc.new{p :bar}) }

  it '.initialize' do
    expect( presenter.instance_variable_get :@name )
        .to eq :foo
    expect( presenter.instance_variable_get :@block )
        .to satisfy { |v| v === Proc.new{p :bar} }
  end

  it '.name' do
    expect( presenter.name )
        .to eq :foo
  end

  it '.block' do
    expect( presenter.block )
        .to satisfy { |v| v === Proc.new{p :bar} }
  end

  it '.eql?' do
    expect( presenter.eql? PRESENTER.new(:foo, Proc.new{p :bar}) )
        .to be_truthy
    expect( presenter.eql? PRESENTER.new(:foo, nil) )
        .to be_truthy
  end

end
