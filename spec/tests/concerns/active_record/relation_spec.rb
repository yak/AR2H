require 'spec_helper'

describe AR2H::Concerns::ActiveRecordConcern::Base do

  it 'respond_to? #presenter' do
    expect(Post.respond_to? :presenter)
        .to be true
  end

  it 'respond_to? #presenters' do
    expect(Post.respond_to? :presenters)
        .to be true
  end

  it 'respond_to? .presenters' do
    expect(Post.first.respond_to? :presenters)
        .to be true
  end

  it 'respond_to? .present' do
    expect(Post.first.respond_to? :present)
        .to be true
  end

end