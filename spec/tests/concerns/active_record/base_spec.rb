require 'spec_helper'

describe AR2H::Concerns::ActiveRecordConcern::Relation do

  it 'respond_to? #present' do
    expect(Post.all.respond_to? :present)
        .to be true
  end

end