require 'spec_helper'

describe AR2H::Executor do

  describe '#run' do
    it 'not correct presenter' do
      expect{ AR2H::Executor.run(User.first, :woohoo) }
          .to raise_error(ArgumentError, /presenter woohoo not found/)
    end
    it 'not correct multiple argument' do
      expect{ AR2H::Executor.run(User.first, :default, 40) }
          .to raise_error(ArgumentError, /Bad multiple argument, expected Boolean but got/)
    end
  end

  describe '.initialize' do
    let (:executor) { AR2H::Executor.new(User.first, AR2H::Presenters::Presenter.new(:foo, nil)) }
    it 'correct' do
      expect( executor.instance_variable_get(:@object) )
          .to eq User.first
      expect( executor.instance_variable_get(:@presenter) )
          .to eql AR2H::Presenters::Presenter.new(:foo, nil)
      expect( executor.instance_variable_get(:@result) )
          .to eq({})
    end
  end

  describe '.execute' do
    let (:executor) { AR2H::Executor.new(User.first, User.first.presenters.find(:one)) }
    it 'correct' do
      expect( executor.run )
          .to eq( {fullname: 'Eugene Yak'} )
    end
  end

  describe '.object' do
    let (:executor) { AR2H::Executor.new(User.first, :default) }
    it '.correct' do
      expect( executor.object )
          .to eq( User.first )
    end
  end

  describe '.attr' do
    let (:comment) { Comment.first }

    it 'existed without block' do
      expect( comment.present(presenter: :existed_without_block) )
          .to eq( {body: 'comment_body_1'} )
    end
    it 'existed_with_block' do
      expect( comment.present(presenter: :existed_with_block) )
          .to eq( {body: 'This text from block'} )
    end
    it 'existed_with_block' do
      expect( comment.present(presenter: :non_existed_without_block) )
          .to eq( {title: nil} )
    end
    it 'existed_with_block' do
      expect( comment.present(presenter: :non_existed_with_block) )
          .to eq( {title: 'This text from block'} )
    end
  end

  describe '.attrs' do
    let (:statistic) { Statistic.first }

    it 'as is' do
      expect( statistic.present(presenter: :as_is) )
          .to eq( {count: 10, last: 12 } )
    end
    it 'one attr does not exist' do
      expect( statistic.present(presenter: :with_a_non_existent_element) )
          .to eq( { count: 10, max: nil } )
    end
  end

  describe '.belongs_to' do
    let (:statistic)                        { Statistic.find 1 }
    let (:statistic_post_not_existed_in_db) { Statistic.find 2 }

    it 'belongs to exist' do
      expect( statistic.present(presenter: :belongs_to) )
          .to eq( {post: {body: 'body'}} )
    end
    it 'belongs to not existed in db' do
      expect( statistic_post_not_existed_in_db.present(presenter: :belongs_to) )
          .to eq( {post: nil} )
    end
    it 'belongs to error' do
      expect{ statistic.present(presenter: :belongs_to_error) }
          .to raise_error(NoMethodError, /undefined method `foo'/)
    end
  end

  describe '.has_one' do
    let (:post_with_statistic)         { Post.find_by(id: 1) }
    let (:statistic_not_existed_in_db) { Post.find_by(id: 2) }

    it 'statistic exist' do
      expect( post_with_statistic.present(presenter: :statistic_exist) )
          .to eq( {statistic: {count: 10, last: 12}} )
    end
    it 'statistic_not_existed_in_db' do
      expect( statistic_not_existed_in_db.present(presenter: :statistic_not_existed_in_db) )
          .to eq( {statistic: nil} )
    end
    it 'statistic_error' do
      expect{ post_with_statistic.present(presenter: :statistic_error) }
          .to raise_error(NoMethodError, /undefined method `error_statistic'/)
    end
  end


  describe '.has_many' do
    let (:post_with_comments)    { Post.find_by(id: 1) }
    let (:post_without_comments) { Post.find_by(id: 2) }

    it 'post with comments' do
      expect( post_with_comments.present(presenter: :post_with_comments) )
          .to eq( {comments: [{body: 'comment_body_1'}, {body: 'comment_body_2'}]} )
    end
    it 'post without comments' do
      expect(post_without_comments.present(presenter: :post_without_comments))
          .to eq( {comments: []} )
    end
    it 'post with error in has many' do
      expect{ post_without_comments.present(presenter: :post_with_error_in_has_many) }
          .to raise_error(NoMethodError, /undefined method `users'/)
    end
  end

end