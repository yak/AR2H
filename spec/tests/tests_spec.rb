require 'spec_helper'

describe AR2H do

  it 'one user' do
    expect( User.first.present(presenter: :one) )
        .to eq( {fullname: 'Eugene Yak'} )
  end

  it 'all users' do
    expect( User.all.present(presenter: :all) )
        .to eq( [{fullname: 'Eugene Yak'}, {fullname: 'Alexander Glad'}] )
  end

end