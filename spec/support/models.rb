require 'active_record'

####################### User #######################

class User < ActiveRecord::Base
  has_many :posts

  presenter do
    attrs :name, :surname
    has_many :posts
  end

  presenter :one do
    attr (:fullname) { "#{object.name} #{object.surname}" }
  end

  presenter :all do
    attr (:fullname) { "#{object.name} #{object.surname}" }
  end
end

####################### Post #######################

class Post < ActiveRecord::Base
  belongs_to :user
  has_many   :comments
  has_one    :statistic

  presenter do
    attr :body
    has_many :comments
    has_one  :statistic
  end

  presenter :for_statistic_test do
    attr (:body) { 'body' }
  end

  presenter :statistic_exist do
    has_one  :statistic, presenter: :default
  end

  presenter :statistic_not_existed_in_db do
    has_one  :statistic, presenter: :default
  end

  presenter :statistic_error do
    has_one  :error_statistic, presenter: :default
  end

  presenter :post_with_comments do
    has_many :comments, presenter: :default
  end

  presenter :post_without_comments do
    has_many :comments, presenter: :default
  end

  presenter :post_with_error_in_has_many do
    has_many :users, presenter: :default
  end

end

###################### Comment #####################


class Comment < ActiveRecord::Base
  belongs_to :post

  presenter do
    attr :body
  end

  presenter :existed_without_block do
    attr :body
  end

  presenter :existed_with_block do
    attr (:body) { 'This text from block' }
  end

  presenter :non_existed_without_block do
    attr :title
  end

  presenter :non_existed_with_block do
    attr (:title) { 'This text from block' }
  end
end

##################### Statistic ####################

class Statistic < ActiveRecord::Base
  belongs_to :post

  presenter do
    attrs :count, :last
  end

  presenter :as_is do
    attrs :count, :last
  end

  presenter :with_a_non_existent_element do
    attrs :count, :max
  end

  presenter :belongs_to do
    belongs_to :post, presenter: :for_statistic_test
  end

  presenter :belongs_to_not_existed_in_db do
    belongs_to :post, presenter: :for_statistic_test
  end

  presenter :belongs_to_error do
    belongs_to :foo, presenter: :for_statistic_test
  end
end