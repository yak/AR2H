# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'AR2H'
  spec.version       = '1.0.0'
  spec.authors       = ['Eugene Yak']
  spec.email         = ['GeneAYak@gmail.com']

  spec.summary       = 'AR2H is a presenter gem for ActiveRecord'
  spec.description   = 'AR2H is a presenter gem for ActiveRecord'
  spec.homepage      = 'https://gitlab.com/yak/AR2H'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler',     '~> 1.13'
  spec.add_development_dependency 'rake',        '~> 10.0'
  spec.add_development_dependency 'rspec',       '~> 3.5'
  spec.add_development_dependency 'sqlite3',     '~> 1.3', '>= 1.3.12'
  spec.add_development_dependency 'simplecov',   '~> 0.12.0'

  spec.add_runtime_dependency     'activerecord'
  spec.add_runtime_dependency     'activesupport'
end
